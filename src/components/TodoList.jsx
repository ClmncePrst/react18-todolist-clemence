import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;
  return (
    <div>
     {/* présente la ToDo List avec le nombre d'items à présenter */} 
      <h1>My todo list ({todos.length} items):</h1>
   {/* la fonction map permet d'afficher les tâches une par une dans une litre non ordonnée */}  
      <ul> 
      {todos.map((todo) => (
          <TodoItem key={todo.id} {...todo} />
      ))}
      </ul>
   {/* l'utilisation d'une key permet d'éviter le message d'erreur en appelant de manire unique chaque élément de "todo" */} 
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
